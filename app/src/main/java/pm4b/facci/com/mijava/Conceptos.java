package pm4b.facci.com.mijava;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class Conceptos extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conceptos);
        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);


        Button btn =  findViewById(R.id.btnJava);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (v.getContext(), Conceptos_Introducion.class);
                startActivityForResult(intent, 0);
            }
        });

        Button btn2 =  findViewById(R.id.btnHello);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (v.getContext(), Conceptos_Hello.class);
                startActivityForResult(intent, 0);
            }
        });

        Button btn3 =  findViewById(R.id.btnComentario);
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (v.getContext(), ConceptosComentarios.class);
                startActivityForResult(intent, 0);
            }
        });

        Button btn4 =  findViewById(R.id.btnVariable);
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (v.getContext(), ConceptosVariable.class);
                startActivityForResult(intent, 0);
            }
        });

        Button btn5 =  findViewById(R.id.btnOPeradores);
        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (v.getContext(), Conceptos_Introducion.class);
                startActivityForResult(intent, 0);
            }
        });

        Button btn6 =  findViewById(R.id.btnIncremento);
        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (v.getContext(), Conceptos_Introducion.class);
                startActivityForResult(intent, 0);
            }
        });

        Button btn7 =  findViewById(R.id.btnString);
        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (v.getContext(), Conceptos_Introducion.class);
                startActivityForResult(intent, 0);
            }
        });

        Button btn8 =  findViewById(R.id.bntObteniendo);
        btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (v.getContext(), Conceptos_Introducion.class);
                startActivityForResult(intent, 0);
            }
        });

    }
}

