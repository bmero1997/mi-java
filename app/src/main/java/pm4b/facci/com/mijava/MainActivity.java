package pm4b.facci.com.mijava;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer =  findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Button btnEmpezar =  findViewById(R.id.btnEmpezar);
        btnEmpezar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (v.getContext(), Conceptos.class);
                startActivityForResult(intent, 0);
            }
        });


    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }



    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        Intent intent;


        int id = item.getItemId();

        if (id == R.id.nav_conceptos) {

            intent = new Intent(MainActivity.this, Conceptos.class);
            startActivity(intent);

        } else if (id == R.id.nav_bucles) {

            intent = new Intent(MainActivity.this, Bucle.class);
            startActivity(intent);

        } else if (id == R.id.nav_arreglos) {

            intent = new Intent(MainActivity.this, Arreglos.class);
            startActivity(intent);

        }else if (id == R.id.nav_clases) {

            intent = new Intent(MainActivity.this, Clases.class);
            startActivity(intent);

        } else if (id == R.id.nav_miperfil) {

            intent = new Intent(MainActivity.this, Perfil.class);
            startActivity(intent);

        } else if (id == R.id.nav_acercade) {

            intent = new Intent(MainActivity.this, Acerca.class);
            startActivity(intent);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
